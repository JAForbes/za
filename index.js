// The Za

const readline = require('readline')

const Ingredient = ({name, stock}) => ({ name, stock })

const rl = readline.createInterface({
	input: process.stdin
	,output: process.stdout
});

const ask = (question) =>
	new Promise(
		Y => rl.question(question, answer => {
			Y(answer.trim())
		})
	)

function init(){

	const ingredients =
		[ 'pepperoni'
		, 'olive'
		, 'prawn'
		, 'zucchini'
		, 'artichoke'
		]
		.map( name => Ingredient({ name, stock: 20 }) )

	const [pepperoni, olive, prawn, zucchini, artichoke] =
		ingredients

	const za = {
		initialBalance: 1000
	}

	const pizzas =
		[{ type: 'pepperoni', ingredients: [pepperoni, olive], cost: 4.5 }
		,{ type: 'marinara', ingredients: [prawn, olive], cost: 6.7 }
		,{ type: 'vegetarian', ingredients: [zucchini, artichoke], cost: 8.9 }
		]

	return {
		menu: 'main' // order | main | pizzas | customers | za
		,za
		,orders: []
		,pizzas
		,ingredients
		,shortcuts: {
			main: {
				O: 'order'
				,T: 'pizzas'
				,C: 'customers'
				,S: 'za'
				,'?': 'main'
				,X: 'exit'
			}
			,order: {}
			,'pizzas': {}
			,'customers': {}
		}
	}
}

const indent = n => (...args) =>
	String.raw(...args)
		.replace(/ +/g, ' ')
		.replace(/\t+/g, Array(n).fill(' ').join(''))

async function update(state){
	console.log('')

	if( state.menu == 'main' ) {
		console.log('The menu choices are')
		console.log(indent(4)`
			O	Order Pizza
			T	Show Pizza Types
			C	Show Customer Orders
			S	Show state of the Za
			?	Show the menu choices.
			X	Exit the system
		`)

		const answer = await ask(`Your choice (O/T/C/S/?/X): `)

		if( answer.toUpperCase() in state.shortcuts.main ){
			return {
				...state
				, menu: state.shortcuts.main[answer.toUpperCase()]
			}
		} else {
			console.log('Sorry that is not a valid choice.')
			return state
		}
	} else if ( state.menu == 'order' ) {
		const name = await ask('Your name: ')
		let pizzas = []
		while ( true ) {
			answer = await ask(indent(4)`Type of pizza (or end): `)

			const pizza = state.pizzas.find( x => x.type == answer )

			if (answer == 'end'){
				break;
			} else if (!pizza) {
				console.log(
					indent(4)
						`We make pepperoni, marinara, and vegetarian pizzas`
				)
			} else {
				pizzas.push(pizza)
			}
		}

		const charge = pizzas.reduce(
			(p,n) => p + n.cost, 0
		)

		console.log(
			indent(4)`The charge is $${charge.toFixed(2)}`
		)

		return {
			...state
			, menu: 'main'
			, orders: state.orders.concat(
				{ customer: name, pizzas, charge }
			)
		}
	} else if (state.menu == 'za'){
		console.log(
			`The Za has $${
				(
					state.za.initialBalance
					+ state.orders.reduce(
						(p,n) => p + n.charge
						, 0
					)
				)
				// .toFixed(2)
			}`
		)
		console.log('The stock on hand is')
		state.ingredients.forEach(x => {
			const stock =
			x.stock -
				state.orders.reduce(
					(p,n) => p + n.pizzas.reduce(
						(p,n) => n.ingredients.map(x => x.name).includes(x.name) ? 1 : 0
						, 0
					)
					, 0
				)
			console.log(
				indent(4)`	${x.name}: ${stock}`
			)
		})
		return { ...state, menu: 'main' }
	} else if ( state.menu == 'customers' ) {
		console.log('The customers served are:')
		state.orders.forEach(
			o => console.log(
				indent(4)
					`${o.customer}: ${o.pizzas.map( x => x.type).join(',')}`
			)
		)
		return { ...state, menu: 'main' }
	} else {
		return { ...state, menu: 'main' }
	}
}

async function main(){
	let state = init()
	while (state.menu != 'exit') {
		state = await update(state)
	}
	rl.close()
	console.log('')
}

main()
	.catch(console.error)